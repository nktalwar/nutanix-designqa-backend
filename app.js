const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient
const app = express();
const port = 4000;
const issuesEndpoints = require('./issues/issues-endpoints');
const versionEndPoints = require('./versions/versions-endpoints')
var db;

app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => res.send('Hello World!'))

MongoClient.connect('mongodb://localhost:27017/', (err, client) => {
  if (err) return console.log(err)
  db = client.db('designQA-1') // whatever your database name isversionEndPoints
  setEndPoints(app,db);
  app.listen(port, () => {
    console.log('listening on ' + port)
  })
});

function setEndPoints (app, db) {
    issuesEndpoints(app, db);
    versionEndPoints(app, db);
}
