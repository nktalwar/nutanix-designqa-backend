module.exports = function(app, db){

    app.post('/newVersion', (req, res) => {
        db.collection('versions').save(req.body, (err, result) => {
            if (err) {
                console.log(err);
                res.status(500).json({
                    'success': false
                });
            } 
            res.status(200).json({
                'success': true,
                'data': [req.body]
            });
          })
    });

    app.get('/versions', (req, res) => {
        let source = req.query.source;
        db.collection('versions').find().sort({ _id : -1 }).toArray(function(err, results) {
            if (err) {
                console.log(err);
                res.status(500).json({
                    'success': false
                });
            } 
            res.status(200).json({
                'success': true,
                'data': source == 'extension' ? formatVerionsForExtension(results) : results
            });
          })
    })
}

function formatVerionsForExtension (results) {
    let _results = [];
    for (let i = 0; i < results.length; i++) {
        let result = results[i];
        _results.push({
            color: convertArrayToObj(result['color']),
            fontSize: convertArrayToObj(result['fontSize']),
            fontWeight: convertArrayToObj(result['fontWeight']),
            fontFamily: convertArrayToObj(result['fontFamily'])
        })
    }
    return _results
}

function convertArrayToObj (arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = 'temp'
     }
     return obj;
}