module.exports = function(app, db){

    app.post('/issues', (req, res) => {
        db.collection('issues').save(req.body, (err, result) => {
            if (err) {
                console.log(err);
                res.status(500).json({
                    'success': false
                });
            } 
            res.status(200).json({
                'success': true,
                'data': []
            });
          })
    });
    
    app.get('/issues', (req, res) => {
        db.collection('issues').find().sort({ _id : -1 }).toArray(function(err, results) {
            if (err) {
                console.log(err);
                res.status(500).json({
                    'success': false
                });
            } 
            res.status(200).json({
                'success': true,
                'data': results
            });
          })
    })

    app.get('/formatedIssues', (req, res) => {
        db.collection('issues').find().sort({ _id : -1 }).toArray(function(err, results) {
            if (err) {
                console.log(err);
                res.status(500).json({
                    'success': false
                });
            }
            res.status(200).json({
                'success': true,
                'data': formattingData(results)
            });
          })
    })
}

let codeString = [
    'function',
    '{',
    '}',
    '(',
    ')',
    'var',
    ';',
    'document',
    '<div',
    '<',
    '/>'
]

function formattingData (results) {
    let _result = []; 
    for (let i = 0; i < results.length; i++) {
        let issues = {};
        let dbIssues = results[i].mainObj.issues;
        for (var key in dbIssues) {
            let dbIssue = dbIssues[key]
            if (!dbIssue[0].issueType) {
                continue;
            }
            issues[dbIssue[0].issueType] = dbIssue;
        }
        let pageInfo = {
            url: results[i].fullUrl,
            createdAt: results[i].issueTime,
            screenshot: results[i].mainObj.screenshot,
            page: results[i].lastUrl
        }
        _result.push({
            pageInfo,
            designIssues: issues,
            pageText: returnTextContent(results[i].pageText || [])
        })
    }
    return _result
}

function returnTextContent (textArray) {
    let _retunTextArray = [];
    for (let i = 0; i < textArray.length; i++) {
        let text = textArray[i]
        let heatLevel = 0;
        for (let j = 0; j < codeString.length; j++) {
            let code = codeString[j];
            if (text.includes(code)) {
                heatLevel++;
            }
        }
        if (heatLevel < 3) {
            _retunTextArray.push(text)
        }
    }
    // _retunTextArray.sort(function(a, b){
    //     // ASC  -> a.length - b.length
    //     // DESC -> b.length - a.length
    //     return b.length - a.length;
    //   });
    return [...new Set(_retunTextArray)]
}
